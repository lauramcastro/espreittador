# EspreiTTador: espreitando o Tuiter con lentes morados!

O EspreiTTador é un resultado do proxecto [(micro)Machismos](http://micromachismos.webs.uvigo.es/), que procura visibilizar ante público a toxicidade das interacións que se producen nas redes sociais coas mulleres como obxectivo.

O EspreiTTador é un buscador que permite explorar publicacións recentes na rede social Twitter sen necesidade dunha conta ou usuario, isto é, libre das condicionantes dos seus algoritmos. Pódense realizar procuras libres, usando un dos termos máis atopados na investigación realizada, ou entre as mencións das persoas obxecto da mesma. 

O EspreiTTador está [despregado en Gigalixir, próbao](https://micromachismos.gigalixirapp.com/)!

![Interface web do EspreiTTador](/priv/static/images/espreittador.png "Interface web do EspreiTTador")

## Instrucións

Hai dous prerrequisitos para poder facer _a túa propia instalación e despregamento_ desta aplicación:

1. O primeiro é ter instalado Phoenix (e todo o necesario para a súa execución). Para facelo, consulta [a documentación](https://hexdocs.pm/phoenix/installation.html#content).

2. O segundo é ter un _bearer token_ para poder facer consultas á API de Twitter. Para obter un, consulta [a documentación](https://developer.twitter.com/en/docs/authentication/oauth-2-0/bearer-tokens). Unha vez o teñas, expórtao na variable de contorna BEARER_TOKEN.

Feito o anterior, poderás clonar este repositorio e executar:

  * `mix deps.get` para descarregar as dependencias
  * `mix phx.server` para lanzar o servidor

Se todo vai ben, deberías poder acceder a [`localhost:4000`](http://localhost:4000) co navegador. Se atopas calquera problema, non teñas reparo en abrir unha _issue_.

