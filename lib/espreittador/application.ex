defmodule Espreittador.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Espreittador.Repo,
      # Start the Telemetry supervisor
      EspreittadorWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Espreittador.PubSub},
      # Start the Endpoint (http/https)
      EspreittadorWeb.Endpoint,
      # Start a worker by calling: Espreittador.Worker.start_link(arg)
      # {Espreittador.Worker, arg}
      {Espreittador.Social.TwitterSearch, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Espreittador.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    EspreittadorWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
