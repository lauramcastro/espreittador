defmodule Espreittador.Poi.Journalists do
  @moduledoc """
  Encapsulates knowledge about the subset of people of interest: journalists
  """

  @doc """
  Returns the list of Twitter names and handles of journalists.
  """
  @spec twitter_all() :: list({String.t(), String.t()})
  def twitter_all() do
    twitter_women() ++ twitter_men()
  end

  @doc """
  Returns the list of Twitter names and handles of women journalists.
  """
  @spec twitter_women() :: list({String.t(), String.t()})
  def twitter_women() do
    [
      {"Cristina García", "cristinagarlop"},
      {"Ana Pastor", "_anapastor_"},
      {"Ana Pardo de Vera", "pardodevera"},
      {"Mónica Carrillo", "MonicaCarrillo"},
      {"Esther Estévez", "esther_estevez_"},
      {"Laura Barrachina", "laurabarrachina"},
      {"Angels Barceló", "AngelsBarcelo"},
      {"Julia Otero", "julia_otero"},
      {"Pepa Bueno", "pepabueno"},
      {"Susana Griso", "susannagriso"}
    ]
  end

  @doc """
  Returns the list of Twitter names and handles of men journalists.
  """
  @spec twitter_men() :: list({String.t(), String.t()})
  def twitter_men() do
    [
      {"Kiko Novoa", "kikonovoa"},
      {"Iñaki López", "_InakiLopez_"},
      {"Ignacio Escolar", "iescolar"},
      {"Vicente Vallés", "VicenteVallesTV"},
      {"Alfonso Hermida", "alfhermida"},
      {"Íñigo Alfonso", "inigoalfonso"},
      {"Carlos Alsina", "carlos__alsina"},
      {"Fernando de Haro", "FernandodeHaro"},
      {"Francisco Marhuenda", "pacomarhuenda"},
      {"Jesús Cintora", "JesusCintora"}
    ]
  end
end
