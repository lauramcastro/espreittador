defmodule Espreittador.Poi.Politicians do
  @moduledoc """
  Encapsulates knowledge about the subset of people of interest: politicians.
  """

  @doc """
  Returns the list of Twitter names and handles of politicians.
  """
  @spec twitter_all() :: list({String.t(), String.t()})
  def twitter_all() do
    twitter_women() ++ twitter_men()
  end

  @doc """
  Returns the list of Twitter names and handles of women politicians.
  """
  @spec twitter_women() :: list({String.t(), String.t()})
  def twitter_women() do
    [
      {"Carmen Silva", "carmelasilva"},
      {"Ana Pontón", "anaponton"},
      {"Lara Méndez", "LaraMendezLopez"},
      {"Yolanda Díaz", "Yolanda_Diaz_"},
      {"Adriana Lastra", "Adrilastra"},
      {"Alexandra Fernández", "aKollontai"},
      {"Andrea Levy", "ALevySoler"},
      {"Ana Oramas", "anioramas"},
      {"Inés Arrimadas", "InesArrimadas"},
      {"Marta Fernández", "martaftapias"}
    ]
  end

  @doc """
  Returns the list of Twitter names and handles of men politicians.
  """
  @spec twitter_men() :: list({String.t(), String.t()})
  def twitter_men() do
    [
      {"Valentín González", "VGFormoso_PSOE"},
      {"Gonzale Caballero", "G_Caballero_M"},
      {"Miguel Anxo Fernández Lores", "Lorespontevedra"},
      {"Alberto Garzón", "agarzon"},
      {"Teodoro García Egea", "TeoGarciaEgea"},
      {"Pablo Arangüena", "PabloAranguena"},
      {"Toni Cantó", "Tonicanto1"},
      {"Miguel Ángel Revilla", "RevillaMiguelA"},
      {"Íñigo Errejón", "ierrejon"},
      {"Gonzalo Trenor", "gonzalotrenor"}
    ]
  end
end
