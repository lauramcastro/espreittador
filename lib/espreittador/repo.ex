defmodule Espreittador.Repo do
  use Ecto.Repo,
    otp_app: :espreittador,
    adapter: Ecto.Adapters.Postgres
end
