defmodule Espreittador.Social.TwitterSearch do
  @moduledoc """
  This module defines the logic for sending a tweet search to Twitter APIv2.

  Implemented as a `GenServer`.
  """
  use GenServer
  use Tesla

  require Logger

  @twitter_api_url "https://api.twitter.com/2"
  @twitter_base_url "https://www.twitter.com/"
  @twitter_api_tweet_search "/tweets/search/recent?query="
  @twitter_api_tweet_search_fields "%20-is%3Aretweet%20-is%3Anullcast&expansions=author_id,entities.mentions.username&tweet.fields=created_at,public_metrics&user.fields=created_at,description,location,name,protected,public_metrics,username,verified"
  @twitter_bearer_token "TWITTER_BEARER_TOKEN"

  @bad_request "Bad Request"

  # API

  @doc """
  Starts the process.
  """
  @spec start_link(any()) :: GenServer.on_start()
  def start_link(_) do
    Logger.info("Starting gen_server process: twitter search")
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Relies a search request to the twitter search process.
  """
  @spec search(String.t()) :: [String.t()]
  def search(what) do
    GenServer.call(__MODULE__, {:search, what})
  end

  # GenServer callbacks

  @impl true
  def init([]) do
    adapter = {Tesla.Adapter.Hackney, [recv_timeout: 30_000]}

    middleware = [
      {Tesla.Middleware.BaseUrl, @twitter_api_url},
      {Tesla.Middleware.BearerAuth, token: System.get_env(@twitter_bearer_token)},
      Tesla.Middleware.JSON
    ]

    {:ok, Tesla.client(middleware, adapter)}
  end

  @impl true
  def handle_call({:search, what}, from, conn) do
    Logger.info("New twitter search task spawned")
    # this can be replaced by a start_link so that we can retry failed tasks
    {:ok, _} = Task.start(__MODULE__, :search_and_reply, [what, from, conn])
    {:noreply, conn}
  end

  @doc """
  Performs a twitter search and sends results back to client.
  """
  @spec search_and_reply(String.t() | {:mentions, String.t()}, {pid(), term()}, Tesla.Client.t()) ::
          :ok
  def search_and_reply({:mentions, user}, client, conn) do
    search_and_reply(user, client, conn)
  end

  def search_and_reply(query, client, conn) do
    results =
      twitter_request(
        conn,
        Enum.join([
          @twitter_api_tweet_search,
          URI.encode(query),
          @twitter_api_tweet_search_fields
        ])
      )
      |> process_response()

    GenServer.reply(client, results)
  end

  defp twitter_request(conn, request) do
    Logger.info("Performing request")

    {:ok, response} = Tesla.get(conn, request)

    response.body
  end

  defp process_response(@bad_request) do
    Logger.info("Request was malformed")
    []
  end

  defp process_response(data) do
    Logger.info("Processing respose")
    do_process_response(data["meta"]["result_count"], data)
  end

  defp do_process_response(0, _context) do
    []
  end

  defp do_process_response(n, context) when is_integer(n) do
    do_process_response(context["data"], context)
  end

  defp do_process_response(data, context) do
    Task.async_stream(data, fn r -> build_result(r, context) end)
    |> Enum.map(fn {:ok, output} -> output end)
  end

  defp build_result(data, context) do
    author_id = data["author_id"]
    user = Enum.find(context["includes"]["users"], fn u -> u["id"] == author_id end)
    {data["text"], @twitter_base_url <> user["username"] <> "/status/" <> data["id"]}
  end
end
