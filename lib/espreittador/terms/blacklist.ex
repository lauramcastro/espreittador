defmodule Espreittador.Terms.Blacklist do
  @moduledoc """
  Encapsulates knowledge about the offensive words of interest.
  """

  @doc """
  Returns the list of offensive terms.
  """
  @spec terms() :: list(String.t())
  def terms() do
    [
      "analfabeta",
      "asco",
      "basura",
      "carcel",
      "cojones",
      "culo",
      "facha",
      "fascista",
      "gentuza",
      "gilipollas",
      "inútil",
      "mentirosa",
      "mierda",
      "puta",
      "sinvergüenza",
      "tonto"
    ]
  end
end
