defmodule EspreittadorWeb.PageLive do
  @moduledoc """
  A module implementing the home page of the application.
  """
  use Phoenix.LiveView, layout: {EspreittadorWeb.LayoutView, "live.html"}

  alias Espreittador.Social.TwitterSearch
  import EspreittadorWeb.Gettext

  @impl true
  def mount(_params, %{"locale" => locale}, socket) do
    Gettext.put_locale(EspreittadorWeb.Gettext, locale)
    {:ok, init(socket)}
  end

  def mount(_param, _session, socket), do: {:ok, init(socket)}

  defp init(socket) do
    socket
    |> assign(results: %{})
    |> assign(offensives: Espreittador.Terms.Blacklist.terms())
    |> assign(
      pois:
        Espreittador.Poi.Journalists.twitter_all() ++ Espreittador.Poi.Politicians.twitter_all()
    )
  end

  @impl true
  def handle_event("search", data, socket) do
    query = extract_query_from_data(data["submittedButton"], data)
    results = TwitterSearch.search(query)
    nresults = length(results)

    {:noreply,
     socket
     |> put_flash(:info, gettext("Showing %{nresults} results", nresults: nresults))
     |> assign(results: results)}
  end

  defp extract_query_from_data("free", data), do: data["q"]
  defp extract_query_from_data("keyword", data), do: data["k"]
  defp extract_query_from_data("person", data), do: {:mentions, data["p"]}
end
