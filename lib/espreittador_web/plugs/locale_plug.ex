defmodule EspreittadorWeb.LocalePlug do
  @moduledoc """
  This module defines a plug for automatically choosing the locale to be used for each user.
  """

  # Copied from
  # https://dev.to/wintermeyer/i18n-with-phoenix-liveview-28mj
  # For a version with cookies and user-language selection, see:
  # https://www.paulfioravanti.com/blog/internationalisation-phoenix-liveview/
  import Plug.Conn

  @locales ["en", "gl"]

  def init(default), do: default

  def call(conn, default) do
    locale = locale_from_header(conn, default)
    Gettext.put_locale(EspreittadorWeb.Gettext, locale)
    conn |> assign(:locale, locale)
  end

  # Taken from set_locale plug written by Gerard de Brieder
  # https://raw.githubusercontent.com/smeevil/set_locale/fd35624e25d79d61e70742e42ade955e5ff857b8/lib/headers.ex
  defp locale_from_header(conn, default) do
    conn
    |> extract_accept_language
    |> Enum.find(default, fn accepted_locale -> Enum.member?(@locales, accepted_locale) end)
  end

  defp extract_accept_language(conn) do
    case Plug.Conn.get_req_header(conn, "accept-language") do
      [value | _] ->
        value
        |> String.split(",")
        |> Enum.map(&parse_language_option/1)
        |> Enum.sort(&(&1.quality > &2.quality))
        |> Enum.map(& &1.tag)
        |> Enum.reject(&is_nil/1)
        |> ensure_language_fallbacks()

      _ ->
        []
    end
  end

  defp parse_language_option(string) do
    captures = Regex.named_captures(~r/^\s?(?<tag>[\w\-]+)(?:;q=(?<quality>[\d\.]+))?$/i, string)

    quality =
      case Float.parse(captures["quality"] || "1.0") do
        {val, _} -> val
        _ -> 1.0
      end

    %{tag: captures["tag"], quality: quality}
  end

  defp ensure_language_fallbacks(tags) do
    Enum.flat_map(tags, fn tag -> remove_country_variant(String.split(tag, "-"), tags) end)
  end

  defp remove_country_variant(tag = [language, _country_variant], tags) do
    if Enum.member?(tags, language), do: [tag], else: [tag, language]
  end

  defp remove_country_variant(tag, _tags), do: tag
end
