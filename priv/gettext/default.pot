## This file is a PO Template file.
##
## "msgid"s here are often extracted from source code.
## Add new translations manually only if they're dynamic
## translations that can't be statically extracted.
##
## Run "mix gettext.extract" to bring this file up to
## date. Leave "msgstr"s empty as changing them here as no
## effect: edit them in PO (.po) files instead.
msgid ""
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:19
#: lib/espreittador_web/live/page_live.html.heex:32 lib/espreittador_web/live/page_live.html.heex:45
msgid "Search Galiverse"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:19
#: lib/espreittador_web/live/page_live.html.heex:32 lib/espreittador_web/live/page_live.html.heex:45
msgid "Searching..."
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:17
msgid "Inspecting the Galiverse with purple glasses"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:18
msgid "Search term"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.ex:30
msgid "Showing %{nresults} results"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:16
msgid "(micro)Machismos: the EspreiTTador"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:23
msgid "EspreiTTador is a result of"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:26
msgid "Feminisms Chair"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:25
msgid "Full project title placeholder"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:25
msgid "funded by"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:26
msgid "in 2021"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:24
msgid "the (micro)Machismos project"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:4
msgid "Custom search"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:14
msgid "Custom search, free from Twitter's algorithm filtering"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:49
msgid "Help us spread the word about our project!"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:50
msgid "If you liked the EspreiTTador, tell the world!"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:15
#: lib/espreittador_web/live/page_live.html.heex:24 lib/espreittador_web/live/page_live.html.heex:37
msgid "Results shown were published in the last 7 days."
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:16
#: lib/espreittador_web/live/page_live.html.heex:25 lib/espreittador_web/live/page_live.html.heex:38
msgid "Retrieves at most 10 matches."
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:5
msgid "Search by keyword"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:23
msgid "Search by one of the top most frequent offensive terms found in our research."
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:6
msgid "Search by person"
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:36
msgid "Search the mentions of one of the persons of interest in our research."
msgstr ""

#, elixir-format
#: lib/espreittador_web/live/page_live.html.heex:7
msgid "Spread the word!"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:29
msgid "EspreiTTador is"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:29
msgid "an open source project"
msgstr ""

#, elixir-format
#: lib/espreittador_web/templates/layout/root.html.heex:30
msgid "registered with the Spanish IP office (reg. num. 03/2022/331)"
msgstr ""
