defmodule Espreittador.TwitterSearchTest do
  use ExUnit.Case
  alias Espreittador.Social.TwitterSearch

  test "search returns a response" do
    results = TwitterSearch.search("whatever")

    assert is_list(results) and
             length(results) != 0
  end
end
