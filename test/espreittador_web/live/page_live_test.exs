defmodule EspreittadorWeb.PageLiveTest do
  use EspreittadorWeb.ConnCase
  import EspreittadorWeb.Gettext

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ gettext("Search term")
    assert render(page_live) =~ gettext("Search term")
  end
end
